﻿(function () {
    'use strict';

    angular.module('funcef-file.controller')
        .controller('NgfFileController', NgfFileController);

    NgfFileController.$inject = ['$attrs', '$scope', 'BASEPATH', '$message'];

    /* @ngInject */
    function NgfFileController($attrs, $scope, BASEPATH, $message) {
        var vm = this;
        vm.basepath = BASEPATH;
        vm.tipos;
        vm.cores;
        vm.corIcone = corIcone;
        vm.verificaIconeArquivo = verificaIconeArquivo;
        vm.excluirArquivo = excluirArquivo;
        vm.tipoImagem = tipoImagem;

        init();

        ////////

        function init() {
            configurarIcones();
            observarFiles();
        }

        function corIcone(file) {
            return vm.cores[getExtensao(file)];
        }

        function verificaIconeArquivo(file) {
            return vm.tipos[getExtensao(file)];
        }

        function getExtensao(file) {
            if (file != undefined && file.Extensao != undefined) {
                return file.Extensao.replace('.', '').toLowerCase();
            }

            if (file.uniqueIdentifier) {
                return file.type.split('/')[1].toLowerCase();
            }
        }

        function tipoImagem(file) {
            var tipo = getExtensao(file);
            return ['png', 'jpeg', 'jpg', 'gif', 'ico'].indexOf(tipo) >= 0 && file.Link;
        }

        function excluirArquivo(nome) {
             $message.confirmacao(
                 'Excluir arquivo',
                 'Deseja realmente excluir o arquivo <strong>' + nome + '</strong>?',
                 removerArquivoService);
        }

        function removerArquivoService() {
            $message.erro('Método ainda não implementado!');
        }

        function configurarIcones() {
            var classe = {
                excel: 'fa-file-excel-o',
                pdf: 'fa-file-pdf-o',
                word: 'fa-file-word-o',
                image: 'fa-file-image-o',
                point: 'fa-file-powerpoint-o'
            };

            var cores = {
                excel: 'bg-green',
                pdf: 'bg-red',
                word: 'bg-primary',
                image: 'bg-gray-light',
                point: 'bg-yellow'
            };

            vm.cores = {
                xlsx: cores.excel,
                xls: cores.excel,
                docx: cores.word,
                doc: cores.word,
                pdf: cores.pdf,
                jpg: cores.image,
                png: cores.image,
                jpeg: cores.image,
                ppt: cores.point,
                pptx: cores.point
            }

            vm.tipos = {
                xlsx: classe.excel,
                xls: classe.excel,
                docx: classe.word,
                doc: classe.word,
                pdf: classe.pdf,
                jpg: classe.image,
                png: classe.image,
                jpeg: classe.image,
                ppt: classe.point,
                pptx: classe.point
            };
        }

        function observarFiles() {
            $scope.$watch('files', function () {
                if (!angular.isArray($scope.files)
                    && $scope.files != undefined) {
                    vm.files = [$scope.files];
                } else {
                    vm.files = $scope.files;
                }
            });
        }
    };
}());