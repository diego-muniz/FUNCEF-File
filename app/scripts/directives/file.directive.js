﻿(function () {
    'use strict';

    angular
      .module('funcef-file.directive')
      .directive('ngfFile', ngfFile);

    /* @ngInject */
    function ngfFile() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/file.view.html',
            controller: 'NgfFileController',
            controllerAs: 'vm',
            scope: {
                files: '='
            }
        };
    }
})();