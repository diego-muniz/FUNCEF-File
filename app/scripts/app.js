﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name File
    * @version 1.0.0
    * @Componente File para listar arquivos
    */
    angular.module('funcef-file.configuration', []);
    angular.module('funcef-file.controller', []);
    angular.module('funcef-file.directive', []);
    


    angular
    .module('funcef-file', [
      'funcef-file.configuration',
      'funcef-file.controller',
      'funcef-file.directive',
      'funcef-message'
    ]);
})();