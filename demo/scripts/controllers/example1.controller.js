﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

        vm.files = [
            {
                CodCrip: 1,
                Nome: 'Arquivo DOC',
                Extensao: '.doc'
            },
            {
                CodCrip: 2,
                Nome: 'Arquivo XLSX',
                Extensao: '.xlsx'
            },
            {
                CodCrip: 3,
                Nome: 'Arquivo PDF',
                Extensao: '.pdf'
            },
            {
                CodCrip: 4,
                Nome: 'Imagem',
                Extensao: 'png',
                Link: 'images/funcef.png'
            }
       ];
    }
}());