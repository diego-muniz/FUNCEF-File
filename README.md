# Componente - FUNCEF-File

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Parâmetros](#parâmetros)
8. [Desinstalação](#desinstalação)


## Descrição

- Componente File é utilizado para carregar arquivos.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-file --save.
```

## Script

```html
<script src="bower_components/funcef-file/dist/funcef-file.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-file/dist/funcef-file.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-file dentro do modulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-file']);
```

## Uso

```html
<ngf-file files="vm.files"></ngf-file>
```

### Parâmetros:

- ngf-file (Obrigatório);

## Desinstalação:

```
bower uninstall funcef-file --save
```