(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name File
    * @version 1.0.0
    * @Componente File para listar arquivos
    */
    angular.module('funcef-file.configuration', []);
    angular.module('funcef-file.controller', []);
    angular.module('funcef-file.directive', []);
    


    angular
    .module('funcef-file', [
      'funcef-file.configuration',
      'funcef-file.controller',
      'funcef-file.directive',
      'funcef-message'
    ]);
})();;angular.module('funcef-file.configuration')
    .constant('BASEPATH', 'http://localhost//');
//.constant('BASEPATH', 'http://desenvintranet/apl/sgcaservico/');

;(function () {
    'use strict';

    angular.module('funcef-file.controller')
        .controller('NgfFileController', NgfFileController);

    NgfFileController.$inject = ['$attrs', '$scope', 'BASEPATH', '$message'];

    /* @ngInject */
    function NgfFileController($attrs, $scope, BASEPATH, $message) {
        var vm = this;
        vm.basepath = BASEPATH;
        vm.tipos;
        vm.cores;
        vm.corIcone = corIcone;
        vm.verificaIconeArquivo = verificaIconeArquivo;
        vm.excluirArquivo = excluirArquivo;
        vm.tipoImagem = tipoImagem;

        init();

        ////////

        function init() {
            configurarIcones();
            observarFiles();
        }

        function corIcone(file) {
            return vm.cores[getExtensao(file)];
        }

        function verificaIconeArquivo(file) {
            return vm.tipos[getExtensao(file)];
        }

        function getExtensao(file) {
            if (file != undefined && file.Extensao != undefined) {
                return file.Extensao.replace('.', '').toLowerCase();
            }

            if (file.uniqueIdentifier) {
                return file.type.split('/')[1].toLowerCase();
            }
        }

        function tipoImagem(file) {
            var tipo = getExtensao(file);
            return ['png', 'jpeg', 'jpg', 'gif', 'ico'].indexOf(tipo) >= 0 && file.Link;
        }

        function excluirArquivo(nome) {
             $message.confirmacao(
                 'Excluir arquivo',
                 'Deseja realmente excluir o arquivo <strong>' + nome + '</strong>?',
                 removerArquivoService);
        }

        function removerArquivoService() {
            $message.erro('Método ainda não implementado!');
        }

        function configurarIcones() {
            var classe = {
                excel: 'fa-file-excel-o',
                pdf: 'fa-file-pdf-o',
                word: 'fa-file-word-o',
                image: 'fa-file-image-o',
                point: 'fa-file-powerpoint-o'
            };

            var cores = {
                excel: 'bg-green',
                pdf: 'bg-red',
                word: 'bg-primary',
                image: 'bg-gray-light',
                point: 'bg-yellow'
            };

            vm.cores = {
                xlsx: cores.excel,
                xls: cores.excel,
                docx: cores.word,
                doc: cores.word,
                pdf: cores.pdf,
                jpg: cores.image,
                png: cores.image,
                jpeg: cores.image,
                ppt: cores.point,
                pptx: cores.point
            }

            vm.tipos = {
                xlsx: classe.excel,
                xls: classe.excel,
                docx: classe.word,
                doc: classe.word,
                pdf: classe.pdf,
                jpg: classe.image,
                png: classe.image,
                jpeg: classe.image,
                ppt: classe.point,
                pptx: classe.point
            };
        }

        function observarFiles() {
            $scope.$watch('files', function () {
                if (!angular.isArray($scope.files)
                    && $scope.files != undefined) {
                    vm.files = [$scope.files];
                } else {
                    vm.files = $scope.files;
                }
            });
        }
    };
}());;(function () {
    'use strict';

    angular
      .module('funcef-file.directive')
      .directive('ngfFile', ngfFile);

    /* @ngInject */
    function ngfFile() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/file.view.html',
            controller: 'NgfFileController',
            controllerAs: 'vm',
            scope: {
                files: '='
            }
        };
    }
})();;angular.module('funcef-file').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/file.view.html',
    "<ul class=\"list-file mt-5\"> <li ng-repeat=\"file in vm.files\"> <a href=\"{{vm.basepath + 'arquivo/downloadFile?codCript=' + file.CodCrip}}\" target=\"_blank\" class=\"btn btn-default\"> <div class=\"{{vm.corIcone(file)}}\" ng-if=\"!vm.tipoImagem(file)\"> <i class=\"icon-file fa {{vm.verificaIconeArquivo(file)}}\"></i> </div> <div class=\"list-file-image\" ng-if=\"vm.tipoImagem(file)\"> <img ng-src=\"{{file.Link}}\" width=\"50\" height=\"auto\"> </div> <div> {{file.Nome ? file.Nome : file.name}} </div> </a> <button ng-click=\"vm.excluirArquivo(file.Nome)\" ng-if=\"editar\" class=\"btn btn-danger btn-xs\"> <i class=\"fa fa-trash\"></i> </button> </li> </ul>"
  );

}]);
